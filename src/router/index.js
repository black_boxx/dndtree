import Vue from 'vue';
import 'vuetify/dist/vuetify.min.css';
import Router from 'vue-router';
import TreeManager from '@/components/TreeManager';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'TreeManager',
      component: TreeManager,
    },
  ],
});
